export class environment {
  static production: boolean = true;
  static api: string = 'http://localhost:3000/';
};
