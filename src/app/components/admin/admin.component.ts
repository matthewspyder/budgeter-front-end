import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
// import { MatPaginator } from '@angular/material/paginator';
// import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { User, UserCreate } from '../../models/user.model';
import { MessageService } from '../../services/message.service';
import { UserService } from '../../services/adminUser.service';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  dataSource = new MatTableDataSource<User>();
  // @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  // @ViewChild(MatSort, { static: true }) sort: MatSort;
  users$!: Observable<User[]>;
  users: User[] = [];
  user!: User;
  selectedUser!: User;
  createdUser: UserCreate = {'email':'email example','username':'username example', 'password':'password example','permissions':'permissions example', 'lighttheme': false};
  displayedColumns: string[] = ['email','username', 'password', 'permissions', 'theme','actions'];
  usersTable$: Observable<MatTableDataSource<User>> | undefined;
  id = new FormControl();
  email=new FormControl();
  permissions=new FormControl();
  theme=new FormControl();

  constructor(private userService: UserService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.usersTable$ = this.userService.getAdminUsers().pipe(
      // switch to new search observable each time the term changes
      map((userspipe: User[]) => {
        this.dataSource.data = userspipe;
        const dataSource = new MatTableDataSource<User>();
        dataSource.data = userspipe;
        return dataSource;
      }
      )
    );
    this.usersTable$.subscribe(test=>{
      this.users = test.data;
      console.log('userstable', this.users);
    });
  }
  public adminTitle = 'Administration Panel';
  updateLocal(form: string, type: string, eventTarget: any, userid: number) {
    if(form==='create'){
      if(type==='password') {
        if (eventTarget.value) {
          this.createdUser.password = eventTarget.value
        }
      } else if(type==='username') {
        if (eventTarget.value) {
          this.createdUser.username = eventTarget.value
        }
      } else if(type==='email') {
        if (eventTarget.value) {
          this.createdUser.email = eventTarget.value
        }
      } else if (type==='permissions') {
        this.createdUser.permissions = eventTarget.value
      } else if (type==='theme') {
        this.createdUser.lighttheme = eventTarget.value
      }
    } else if(form === 'update') {
      if(type==='email') {
        if (eventTarget.value) {
          let tmp = this.getUserByKey(userid);
          tmp.email = eventTarget.value
          this.setUserByKey(tmp, userid);
        }
      } else if(type==='username') {
        if (eventTarget.value) {
          let tmp = this.getUserByKey(userid);
          tmp.username = eventTarget.value
          this.setUserByKey(tmp, userid);
        }
      } else if(type==='password') {
        if (eventTarget.value) {
          let tmp = this.getUserByKey(userid);
          tmp.password = eventTarget.value
          this.setUserByKey(tmp, userid);
        }
      } else if(type === 'permissions') {
        let tmp = this.getUserByKey(userid);
        tmp.permissions = eventTarget.value;
        this.setUserByKey(tmp, userid);
      } else if(type === 'theme') {
        let tmp = this.getUserByKey(userid);
        tmp.lighttheme = eventTarget.value;
        this.setUserByKey(tmp, userid);
      }
      // console.log(this.users[userid]);
    }

  }
  updateUser(userId: number): void {
    this.userService.updateAdminUser(this.getUserByKey(userId)).subscribe(user2 => {
      console.log(user2)
    });
  }
  setUserByKey(user: User, userId: number) : User {
    for(let i=0;i<this.users.length; i++){
      if(this.users[i].userid === userId){
        this.users[i]=user;
      }
    }
    return this.users[0];
  }
  getUserByKey(userId: number) : User {
    for(let i=0;i<this.users.length; i++){
      if(this.users[i].userid === userId){
        return this.users[i];
      }
    }
    return this.users[0];
  }
  createUser(): void {
    if(this.createdUser.email!=='email example'){
      if(this.createdUser.password!=='username example') {
        if(this.createdUser.password!=='password example') {
          if(this.createdUser.permissions!=='permissions example'){
            this.userService.createAdminUser(this.createdUser).subscribe(users2 => {
              console.log(users2)
            });
          } else {
            console.log('Please Add Permissions');
          }
        } else {
          console.log('Please Add Password');
        }
      } else {
        console.log('Please Enter Username');
      }
    } else {
      console.log('Please Add Email');
    }
  }
  getUser(userid: number): void {
    this.userService.getAdminUser(userid).subscribe(users2 => {
      this.selectedUser = users2; console.log(users2)
    });
  }
  removeUser(userid: number): void {
    this.userService.removeAdminUser(userid).subscribe(users2 => {
      this.selectedUser = users2; console.log(users2)
    });
  }

}
