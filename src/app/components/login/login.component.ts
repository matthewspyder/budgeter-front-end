import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router'
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username = new FormControl();
  password = new FormControl();

  constructor(private router: Router,
    private loginService: LoginService,
    private globalService: GlobalService) { }

  ngOnInit(): void {
    if(this.globalService.checkSession()){
      this.router.navigate(['/dashboard']);
    }
  }

  login(): void {
    if(this.username.value){
      if(this.password.value) {
        this.loginService.login(this.username.value,this.password.value).subscribe(response => {
          console.log('Response ', response);
          this.globalService.setSession(response.session);
          this.router.navigate(['/dashboard']);
        });
      } else {
        console.log('Please Enter Password');
      }
    } else {
      console.log('Please Enter Username');
    }
  }

}
