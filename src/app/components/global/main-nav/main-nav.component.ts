import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../services/global.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements OnInit {

  constructor(
    private globalService: GlobalService,
    private router: Router
  ) { }

  ngOnInit(): void {

  }

  showFiller = true;

  public toggleTheme(){
    this.globalService.toggleTheme();
  }

  public isLight(){
    return this.globalService.isLightTheme();
  }

  public isLoggedIn(){
    return this.globalService.isLoggedIn();
  }

  public routeTo(link: string){
    // Create Book logic
    this.router.navigate([link]);
 }
 public logout() {
   this.globalService.logout();
 }
 public getSideNavStatus () {
    return this.globalService.getSideNavStatus();
 }
 public toggleSideNav () {
   this.globalService.toggleSideNavStatus();
 }
 public getUsername() {
   return this.globalService.getSession().userInfo.username;
 }
}
