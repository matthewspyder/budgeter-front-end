import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ErrorDialogService } from 'src/error-dialog/errordialog.service';
import { environment } from 'src/environments/environment';
import { GlobalService } from '../services/global.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  public envurl = environment.api;

  constructor(public errorDialogService: ErrorDialogService,
    private globalService: GlobalService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      if(this.globalService.checkSession()) {
        request = request.clone({ headers: request.headers.set('X-JWT-Token', this.globalService.getSession().token) });
      }
      /*request = request.clone({ headers: request.headers.set('Access-Control-Allow-Origin', this.envurl) });
      request = request.clone({ headers: request.headers.set('Access-Control-Allow-Credentials', 'true') });
      request = request.clone({ headers: request.headers.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept') });
      request = request.clone({ headers: request.headers.set('Access-Control-Allow-Methods', 'GET, POST') });
      request = request.clone({ headers: request.headers.set('Postman-Token', '1ceeb2a8-587e-40dc-949f-17a0c1a439a5') });*/
      // request = request.clone({ headers: request.headers.set('Accept', '*/*') });

      if(!request.headers.has('Content-Type')) {
        request = request.clone({ headers: request.headers.set('Content-Type', 'application/json; charset=utf-8') });
      }
      if(!request.headers.has('Content-Length')){
        request = request.clone({ headers: request.headers.set('Content-Type', 'application/json; charset=utf-8') });
      }

      return next.handle(request).pipe(
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
              console.log('event--->>>', event);
          }
          return event;
      }),
      catchError((error: HttpErrorResponse) => {
        let data = {};
        data = {
            reason: error && error.error && error.error.message ? error.error.message : '',
            status: error.status
        };
        this.errorDialogService.openDialog(data);
        return throwError(error);
    })
      )
    }
}
