export interface IUser {
  userid: number;
  username: string;
  createddate: string;
  email: string;
  permissions: string;
  lighttheme: boolean;
}
export class User implements IUser
{
   constructor(public userid: number,
    public username: string,
    public createddate: string,
    public email: string,
    public password: string,
    public permissions: string,
    public lighttheme: boolean)
	{
	}
  // example = {'userid': -1,'email':'email example','permissions':'permissions example', 'lighttheme': false};
	/*calculateDiscount(percent: number): number {
	return this.price - (this.price * percent / 100);
	}*/
}
export interface IUserCreate {
  email: string;
  username: string;
  password: string;
  permissions: string;
  lighttheme: boolean;
}
export class UserCreate implements IUserCreate
{
   constructor(public email: string,
    public username: string,
    public password: string,
    public permissions: string,
    public lighttheme: boolean)
	{
	}
  // example = {'userid': -1,'email':'email example','permissions':'permissions example', 'lighttheme': false};
	/*calculateDiscount(percent: number): number {
	return this.price - (this.price * percent / 100);
	}*/
}
