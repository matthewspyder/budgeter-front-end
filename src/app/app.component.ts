import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'budgeter-front-end';
  constructor(private globalService: GlobalService) {
  }
  public getLightTheme() {
    return this.globalService?.isLightTheme()
  }
}

