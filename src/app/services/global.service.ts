import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Session } from '../models/session.model';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  private lightTheme = false;
  private sideNavStatus = false;
  constructor(
    private router: Router,
    private cookieService: CookieService
    ) { }

  public isLightTheme() {
    return this.lightTheme;
  }
  public toggleTheme() {
    this.lightTheme = !this.lightTheme;
  }
  public isLoggedIn(){
    if(this.cookieService.get('session'))
      return true;
    else
      return false;
  }
  public logout() {
    // add route to invalid token
    this.cookieService.deleteAll();
    this.router.navigate(['/login']);
    if(this.sideNavStatus) {
      this.toggleSideNavStatus();
    }
  }
  checkSession() {
    return this.cookieService.check('session');
  }
  getSession() {
    if(this.checkSession())
      return JSON.parse(this.cookieService.get('session'));
    else
      return ''
  }
  setSession(session: Session) {
    console.log('Set Session ', session);
    this.cookieService.set('session',JSON.stringify(session),session.expires)
  }
  getSideNavStatus() {
    return this.sideNavStatus;
  }
  toggleSideNavStatus() {
    this.sideNavStatus = !this.sideNavStatus;
  }
}
