import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, catchError, tap } from 'rxjs';
import { MessageService } from './message.service';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public loginURL = environment.api;

  constructor(
    private messageService: MessageService,

    private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    const url = `${this.loginURL+'login'}`;
    return this.http.post<any>(url,{username,password})
    .pipe(
      tap(_ => this.messageService.log(`fetched hero id=`)),
      catchError(this.messageService.handleError<any>(`getHero id=`))
    );
  }
}
