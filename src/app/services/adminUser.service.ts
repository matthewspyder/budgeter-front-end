import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { User, UserCreate } from '../models/user.model';
import { Observable, of, catchError, map, tap } from 'rxjs';
import { MessageService } from './message.service';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  public userURL = environment.api+'admin/user';

  constructor(
    private messageService: MessageService,
    private http: HttpClient) {
    }

    getAdminUsers(): Observable<User[]> {
      const url = this.userURL+'/all';
      return this.http.get<User[]>(url)
      .pipe(
        catchError(this.messageService.handleError<any>('getUsers'))
      )
    }
    getAdminUser(userid: number): Observable<User> {
      const url = `${this.userURL}/${userid}`;
      return this.http.get<User>(url)
      .pipe(
        tap(_ => this.messageService.log(`fetched hero id=${userid}`)),
        catchError(this.messageService.handleError<any>(`getHero id=${userid}`))
      );
    }
    updateAdminUser(user: User): Observable<User> {
      const url = `${this.userURL}`;
      return this.http.put<User>(url,user)
      .pipe(
        tap(_ => this.messageService.log(`fetched hero id=${user.userid}`)),
        catchError(this.messageService.handleError<any>(`getHero id=${user.userid}`))
      );
    }
    createAdminUser(user: UserCreate): Observable<User> {
      const url = `${this.userURL}`;
      return this.http.post<User>(url,user)
      .pipe(
        tap(_ => this.messageService.log(`fetched hero id=`)),
        catchError(this.messageService.handleError<any>(`getHero id=`))
      );
    }
    removeAdminUser(userid: number) {
      const url = `${this.userURL}/${userid}`;
      return this.http.delete<User>(url)
      .pipe(
        tap(_ => this.messageService.log(`fetched hero id=${userid}`)),
        catchError(this.messageService.handleError<any>(`getHero id=${userid}`))
      );
    }
}
